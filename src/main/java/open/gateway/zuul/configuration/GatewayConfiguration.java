package open.gateway.zuul.configuration;

import open.gateway.zuul.filters.PreFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {

    @Bean
    public PreFilter simpleFilter() {
        return new PreFilter();
    }
}
